<?php

namespace App\Repository;

use App\entities\Person;
use PDO;


class PersonRepository{

    private PDO $connection;
    public function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=p23_first', 'simplon', '1234');
    }

    /**
     * Summary of findAll Faire une requête SQL vers la base de données et convertir les résultats de cette requête en instances de la classe Person
     * @return Person[]
     */
    public function findAll(int $pgNb = 1, int $nb = 15): array{
    
        $personArray = [];
        if($nb <= 0){
            $first = 0;
        } else {
            $first = ($pgNb * $nb) - $nb;
        }
        
        $statement = $this->connection->prepare('SELECT * FROM person LIMIT :usedNb OFFSET :first');
        $statement->bindValue('first', $first, PDO::PARAM_INT);
        $statement->bindValue('usedNb', $nb, PDO::PARAM_INT); 
        $statement->execute();
        
        $results = $statement->fetchAll();
        
        foreach($results as $item){
            $personArray[] = new Person($item['firstname'], $item['name'], $item['id']);
        }
        return $personArray;
    } 

    public function persist(Person $person){

        $statement = $this->connection->prepare('INSERT INTO person (firstname, name) VALUES ( :firstname , :name)');

        $statement->bindValue('firstname',$person->getFirstname());
        $statement->bindValue('name', $person->getName());
        
        $statement->execute();
        $person->setId($this->connection->lastInsertId());
        
    }
}