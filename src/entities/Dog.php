<?php

namespace App\entities;

use DateTime;

class Dog {

    private ?int $id;
    private string $name;

    private string $breed;

    private ?DateTime $birthdate;



    /**
     * @param string $name
     * @param string $breed
     * @param DateTime $birthdate
     * @param ?int $id
     */
    public function __construct(string $name, string $breed, ?DateTime $birthdate = null, ?int $id = null) {
    	$this->name = $name;
    	$this->breed = $breed;
    	$this->birthdate = $birthdate;
        $this->id = $id;
    }
    

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getBreed(): string {
		return $this->breed;
	}
	
	/**
	 * @param string $breed 
	 * @return self
	 */
	public function setBreed(string $breed): self {
		$this->breed = $breed;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getBirthdate(): ?DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime|null $birthdate 
	 * @return self
	 */
	public function setBirthdate(?DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}

	/**
	 * @return ?int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param ?int $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
}